@resetDb
Feature:
  In order to retrieve User data
  As a logged user
  I want to list, show, create, update and delete a User

  Scenario: As an admin user, I can retrieve the list of users
    When I login as "admin@example.com"
    When I add "Accept" header equal to "application/ld+json"
    When I send a "GET" request to "/api/users?page=1"
    Then the response status code should be 200
    And the JSON node "@id" should be equal to "/api/users"
    And the JSON node "@type" should be equal to "hydra:Collection"
    And the JSON node "hydra:totalItems" should be equal to 3

  Scenario: As an regular user, I cannot retrieve the list of users
    When I login as "user@example.com"
    When I add "Accept" header equal to "application/json"
    When I send a "GET" request to "/api/users"
    Then the response status code should be 403

  Scenario: As a anonymous user, I cannot retrieve the list of users
    When I send a "GET" request to "/api/users"
    Then the response status code should be 401


  Scenario: As a logged user, I can retrieve my details
    When I login as "user@example.com"
    When I add "Accept" header equal to "application/ld+json"
    When I send a "GET" request to "/api/users/2"
    Then the response status code should be 200
    And the JSON node "id" should be equal to 2
    And the JSON node "email" should be equal to "user@example.com"
    And the JSON node "username" should be equal to "basic user"
    And the JSON node "roles" should exist
    And the JSON node "enabled" should be equal to "true"
    And the JSON node "createdAt" should exist
    And the JSON node "updatedAt" should exist

  Scenario: As a logged user, I cannot retrieve details of another users
    When I login as "user@example.com"
    When I add "Accept" header equal to "application/json"
    When I send a "GET" request to "/api/users/3"
    Then the response status code should be 403

  Scenario: As a anonymous user, I cannot retrieve details about one users
    When I add "Accept" header equal to "application/json"
    When I send a "GET" request to "/api/users/1"
    Then the response status code should be 401
