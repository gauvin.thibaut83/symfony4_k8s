<?php

use Behat\Behat\Context\Environment\InitializedContextEnvironment;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behatch\Context\RestContext;

trait ContextAwareTrait
{
    private $contexts;

    /**
     * @BeforeScenario
     *
     * @param BeforeScenarioScope $scope
     *
     * @throws ReflectionException
     */
    public function resolveContexts(BeforeScenarioScope $scope): void
    {
        /** @var InitializedContextEnvironment $environment */
        $environment = $scope->getEnvironment();
        $contextsProperty = new \ReflectionProperty(get_class($environment), 'contexts');
        $contextsProperty->setAccessible(true);

        $this->contexts = $contextsProperty->getValue($environment);
    }

    public function getContext(string $name)
    {
        return $this->contexts[$name];
    }

    public function getRestContext(): RestContext
    {
        return $this->getContext(RestContext::class);
    }
}
