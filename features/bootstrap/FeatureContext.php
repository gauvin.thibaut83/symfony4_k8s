<?php

use App\Entity\User;
use App\Repository\UserRepository;
use Behat\Behat\Context\Context;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;

class FeatureContext implements Context
{
    use ContextAwareTrait;

    /** @var UserRepository */
    private $userRepository;

    /** @var JWTManager */
    private $jwtManager;

    /**
     * FeatureContext constructor.
     *
     * @param JWTManager $jwtManager
     * @param UserRepository $userRepository
     */
    public function __construct(JWTManager $jwtManager, UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->jwtManager = $jwtManager;
    }

    /**
     * @When I login as :email
     */
    public function iLoginAs(string $email): void
    {
        $token = $this->getAccessToken($email);

        $restContext = $this->getRestContext();
        $restContext->iAddHeaderEqualTo('Authorization', $token);
    }

    /**
     * @param string $email
     *
     * @return string
     */
    private function getAccessToken(string $email): string
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['email' => $email]);

        return sprintf('Bearer %s', $this->jwtManager->create($user));
    }

    /**
     * @BeforeFeature
     *
     * @resetDb
     */
    public static function createDb(): void
    {
        shell_exec(self::getSymfonyCommand('doctrine:database:create'));
        shell_exec(self::getSymfonyCommand('doctrine:migrations:migrate --no-interaction'));
        shell_exec(self::getSymfonyCommand('hautelook:fixtures:load --no-interaction'));
    }

    /**
     * @AfterFeature
     */
    public static function dropDb(): void
    {
        shell_exec(self::getSymfonyCommand('doctrine:database:drop --force --if-exists'));
    }

    /**
     * @param string $command
     *
     * @return string
     */
    private static function getSymfonyCommand(string $command): string
    {
        return sprintf('APP_ENV=test php bin/console %s', $command);
    }
}
