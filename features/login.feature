@resetDb
Feature:
  In order to use API
  As a user
  I want to login with my credentials and retrieve a valid JWT

  Scenario: I retrieve a valid JWT using valid credentials
    When I add "Content-Type" header equal to "application/json"
    When I send a "POST" request to "/login" with body:
    """
    {
        "email": "user@example.com",
        "password": "password"
    }
    """
    Then the response status code should be 200
    And the JSON node "token" should exist
