<?php
$currentYear = date('Y');

$licenceHeader = <<<"HEADER"
NOTICE OF LICENSE

This source file is licensed exclusively to @ACME CORP

@copyright   Copyright © 2019-{$currentYear} @ACME CORP
@license     All rights reserved

NOTICE OF LICENSE
HEADER;

return PHPCsFixer\Config::create()
    ->setRiskyAllowed( true )
    ->setRules([
        '@Symfony'                   => true,
        'header_comment'             => [
            'header'      => $licenceHeader,
            'commentType' => 'PHPDoc',
            'separate'    => 'bottom'
        ],
        'array_syntax' => ['syntax' => 'short'],
        'no_superfluous_phpdoc_tags' => true,
    ])
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in('src')
            ->in('tests')
    );
