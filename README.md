## K8S tutorial notes

[![build status](https://gitlab.com/gauvin.thibaut83/symfony4_k8s/badges/master/pipeline.svg)](https://gitlab.com/gauvin.thibaut83/symfony4_k8s/pipelines)

#### Requirements:
- kubectl
- docker & docker-compose

---

#### Usage:
- Start local k3s services
    ```
    $ make k3s.start
    ```

- Display running services for `symfony4-k8s-prod` namespace
    ```
    $ make k3s.infos
    ```

- Deploy provisioner for `hostPath` based persistent volume on the k3s nodes
    ```
    $ make k3s.create-local-storage
    ```
    For more infos about this persistent volume, check  
    https://github.com/rancher/local-path-provisioner

- Deploy Symfony4 skeleton app on local k3s nodes
    ```
    $ make deploy
    ```
    This can take a long time, depending on your internet connection.

- Populate database
    ```
    # Attach to php container in local k3s node context
    $ make exec.php
    
    # Install dev dependencies
    ~> composer install --dev
    
    # Populate database with dev fixtures
    ~> APP_ENV=dev php bin/console hautelook:fixtures:load -n
    ```

---

#### Updating pod deployment

- Note that the postgresql database is persisted between each release
- On each push, trigger the [release job](https://gitlab.com/gauvin.thibaut81/symfony4_k8s/pipelines) in the CI to build docker image to release locally on your k3s cluster
- Then, use `$ make deploy.patch` target to update Web deployment
- Follow deployment update `$ make k3s.infos`

---

#### Urls

- Visit [homepage](http://symfony4-k8s.localhost/)
- Visit [API docs](http://symfony4-k8s.localhost/api/docs)

- Get valid JWT
    ```bash
    curl -X POST http://symfony4-k8s.localhost/login \
        -H 'Content-Type: application/json' \
        -H 'Host: symfony4-k8s.localhost' \
        -d '{
            "email": "user@example.com",
            "password": "password"
        }'

    # Response
    {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NTg4MDM4NTEsImV4cCI6MTU1ODgwNzQ1MSwicm9sZXMiOlsiUk9MRV9VU0VSIl0sImVtYWlsIjoidXNlckBleGFtcGxlLmNvbSJ9.UIMNpc0xTP5onwCQ8yTzGClFvWHWUq99-rq68hGj54AxFnyVes3nCj2bUIi2zj5pOfYDmsmN_055Kyc0afEtNWmBnAgiDxX1ue3e4VtjvKGtkSF3I-1sn-mWYSq-AbLM3ZNFcbiBuescU7unm1YAGK3Jey8lJvVyWDDdpQ1XGipH6LISpYAA_uc27oAlfsgdx-a3Z6Mjv2AruPeTDya-2TKUKt9hikkKqV6Q-Xd5aOQ0NfRhvKyaZBm7Z-xaLZozzSPQ-2xfqiNvdjdrJv_0ir2xFh0zM_C3veF6apRfmbCSiKG5fjcIZiv841PQQ-QgJna4x0JsgPZd-ULu8KVD58uK5Ow5soC-3Oo9kB6yxgs7Pkk-sjJZLxkpRh7RM8DsRa3-KDTcd1I0iy02hX634UrfNKmIgJMM1xP2j0vt5g82eo1wO2uDAxiypfpXpZ0TkKz3kCqcJkphrWw_Rll5VtqN0nYZD2gzUFK40PItUkWJ-Qphjkfc4_ZL0HTky4eFTc2U6CfLSbcDzVDbzpFxvT_cexkW1FcKDfBQYlu5F18zzyBHRGxPFU4IRI1pcnWNd0PNyX6vTDzy4I5hhML3K5m-rLUeDZWkIezZtJNw6zccaA_YqtwlbsvNFF8MfqxUUrLNgV4l25zO_LuvvhU5FUC8EF0oQlHhxV75_ZZr5sc"
    }
    ```

    Then, add http header "Authorisation: Bearer {JWT_TOKEN_VALUE}" to your API request to authenticate api calls
    ```bash
    # Example
    curl -X GET http://symfony4-k8s.localhost/api/users -H 'Authorization: Bearer JWT_TOKEN_VALUE'

    # Response
    {
        "@context": "/api/contexts/User",
        "@id": "/api/users",
        "@type": "hydra:Collection",
        "hydra:member": [
            {
                "@id": "/api/users/1",
                "@type": "User",
                "id": 1,
                "email": "user@example.com",
                "username": "user example",
                "roles": [
                    "ROLE_USER"
                ],
                "enabled": true
            }
        ],
        "hydra:totalItems": 1
    }
    ```


#### Add behat stack for api testing

```bash
$ make ssh

~> composer req --dev behat/behat
~> composer req --dev behat/mink:dev-master # --> for compliance with symfony/css-selector
~> composer req --dev behat/mink-extension
~> composer req --dev behat/mink-browserkit-driver
~> composer req --dev behat/symfony2-extension
~> composer req --dev behatch/contexts
```
