<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is licensed exclusively to @ACME CORP
 *
 * @copyright   Copyright © 2019-2019 @ACME CORP
 * @license     All rights reserved
 *
 * NOTICE OF LICENSE
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login", methods={"POST"})
     */
    public function login(Security $security)
    {
        return $security->getUser();
    }
}
