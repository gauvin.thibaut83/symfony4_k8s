<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is licensed exclusively to @ACME CORP
 *
 * @copyright   Copyright © 2019-2019 @ACME CORP
 * @license     All rights reserved
 *
 * NOTICE OF LICENSE
 */

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @Template("home/index.html.twig")
     */
    public function index(): array
    {
        return [];
    }
}
