<?php

declare(strict_types=1);
/**
 * NOTICE OF LICENSE
 *
 * This source file is licensed exclusively to @ACME CORP
 *
 * @copyright   Copyright © 2019-2019 @ACME CORP
 * @license     All rights reserved
 *
 * NOTICE OF LICENSE
 */

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190526221152 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Update Users table: Add created_at & updated_at field';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE users ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE users ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE users DROP created_at');
        $this->addSql('ALTER TABLE users DROP updated_at');
    }
}
