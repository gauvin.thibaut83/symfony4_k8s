<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is licensed exclusively to @ACME CORP
 *
 * @copyright   Copyright © 2019-2019 @ACME CORP
 * @license     All rights reserved
 *
 * NOTICE OF LICENSE
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     accessControl="has_role('ROLE_USER')",
 *     itemOperations={
 *          "get"={
 *              "access_control"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') and object == user)",
 *              "access_control_message"="You are not allow to see others users data"
 *          }
 *     },
 *     collectionOperations={
 *         "get"={
 *              "access_control"="is_granted('ROLE_ADMIN')",
 *              "access_control_message"="Only admin can list users data"
 *          }
 *     },
 *     normalizationContext={"groups"="read"}
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User implements UserInterface
{
    use TimestampableEntity;

    /**
     * @var int The id of this user
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @var string The user email
     *
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @Assert\NotBlank
     * @Assert\Email
     */
    private $email;

    /**
     * @var string|null The username
     *
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @var array|null The user roles
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string|null The user encoded password
     *
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @var bool|null Is this user activated?
     *
     * @ORM\Column(type="boolean", options={"default"=false})
     */
    private $enabled = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): User
    {
        $this->username = $username;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(?array $roles): User
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): User
    {
        $this->password = $password;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): User
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getSalt()
    {
    }

    public function eraseCredentials()
    {
    }
}
