DOCKER_COMPOSE=docker-compose
DOCKER_RUN_PHP=$(DOCKER_COMPOSE) run --rm php
LOCAL_USER_ID=$(shell id -u $$USER)
KUBECTL_CMD=kubectl --kubeconfig=.data/k3s/kubeconfig.yaml -n symfony4-k8s-prod
K8S_POD_WEB_NAME=$(shell ${KUBECTL_CMD} get pods | egrep '^web\-[a-z0-9]{10}-[a-z0-9]{5}' | cut -f 1 -d ' ')


##
## ---------------------
## Available make target
## ---------------------
##


##
## Common
## ------
##

all: help
help: ## Display this message
	@grep -E '(^[a-zA-Z0-9_.-]+:.*?##.*$$)|(^##)' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

install: start ## Install app
	@$(DOCKER_RUN_PHP) bash -c '\
		composer install --no-interaction; \
		./bin/console d:m:m -n; \
	'

start: ## Start containers
	@DOCKER_USER_ID=$(LOCAL_USER_ID) $(DOCKER_COMPOSE) up -d --build --remove-orphans --no-recreate
	@$(DOCKER_COMPOSE) ps

stop: ## Stop containers
	@$(DOCKER_COMPOSE) down

restart: stop start ## Restart containers

ssh: ## Start new bash terminal inside the php container
	@$(DOCKER_RUN_PHP) bash

logs: ## Logs containers
	@$(DOCKER_COMPOSE) logs -f

generate.jwt-pem: ## Generate couples of RSA keys to encrypt JWT
	@echo "\033[0;32mHere is the jwt passphrase:" $(shell grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')"\033[0m"
	mkdir -p config/jwt
	openssl genrsa -out config/jwt/private.pem -aes256 4096
	openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
	@echo "\033[0;32mYour keys is ready to use !\033[0m"
	@ls -alh config/jwt


##
## Database
## ------
##

db.shell: ## Run new psql session
	@$(DOCKER_COMPOSE) exec db psql -U devops -d symfony4_skeleton


##
## Quality
## ------
##

lint.fix: ## Lint all src folder
	@$(DOCKER_RUN_PHP) vendor/bin/php-cs-fixer fix

lint.fix.git: ## Lint only modified files
	@$(DOCKER_RUN_PHP) bash -c ' \
		for file in `git diff --name-only --diff-filter=R --diff-filter=M --diff-filter=A HEAD`; do \
			if [ "$${file##*.}" = "php" ]; then \
				vendor/bin/php-cs-fixer fix $$file --config .php_cs; \
				echo $$file; \
			fi\
		done; \
    '

##
## Tests
## ------
##

test: test.phpunit test.behat ## Run all test

test.behat: ## Run Behat test suite
	@$(DOCKER_RUN_PHP) vendor/bin/behat -f progress

test.behat-wip: ## Run Behat tests who are tagged with "@wip"
	@$(DOCKER_RUN_PHP) vendor/bin/behat --tags=@wip

test.phpunit: ## Run Phpunit test suite
	@$(DOCKER_RUN_PHP) bin/phpunit -c phpunit.xml.dist

##
## Infra
## ------
##

deploy: ## Deploy images on local k3 cluster
	${KUBECTL_CMD} apply -f .kubernetes/env/prod/namespace.yml,.kubernetes/env/prod/ingress.yml,.kubernetes/env/prod/service-web.yml,.kubernetes/env/prod/secret-postgres.yml,.kubernetes/env/prod/secret-web.yml,.kubernetes/env/prod/configmap-web.yml,.kubernetes/env/prod/persistent-volume-claim.yml,.kubernetes/env/prod/deployment.yml

# Image to use for patch Web deployment
NGINX_IMG_NAME = "registry.gitlab.com/gauvin.thibaut83/symfony4_k8s/nginx:prod-latest"
PHP_IMG_NAME   = "registry.gitlab.com/gauvin.thibaut83/symfony4_k8s/php:prod-latest"

deploy.patch: ## Patch web deployment
	${KUBECTL_CMD} patch deployment web --patch '{"spec": {"template": {"spec": {"containers": [{"name": "nginx", "image": ${NGINX_IMG_NAME}},{"name": "php-fpm", "image": ${PHP_IMG_NAME}}]}}}}'

log.nginx: ## Display logs of NGINX container in k3s web pod
	${KUBECTL_CMD} -c nginx logs -f ${K8S_POD_WEB_NAME}

log.php: ## Display logs of PHP container in k3s web pod
	${KUBECTL_CMD} -c php-fpm logs -f ${K8S_POD_WEB_NAME}

exec.php: ## Exec to PHP container in k3s web pod
	${KUBECTL_CMD} -c php-fpm exec -ti ${K8S_POD_WEB_NAME} bash


##
## Kubernetes
## ----------
##

k3s.start: ## Start k3s dev server
	@$(DOCKER_COMPOSE) -f .kubernetes/docker-compose-k3s.yml up -d
	@$(DOCKER_COMPOSE) -f .kubernetes/docker-compose-k3s.yml ps

k3s.stop: ## Stop k8s dev server
	@$(DOCKER_COMPOSE) -f .kubernetes/docker-compose-k3s.yml down

k3s.infos: ## Get current pods
	${KUBECTL_CMD} get namespace,secret,configmap,deployment,svc,ingress,pod,replicaset,job,cronjob,pv,pvc

k3s.create-local-storage:  ## Deploy provisioner for hostPath based persistent volume
	kubectl --kubeconfig=.data/k3s/kubeconfig.yaml apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml
